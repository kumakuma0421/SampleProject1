# GitPractice

// ---------------------------------------------------------------------------
// MakeMyDay(day1:int, day2:int):bool
// ---------------------------------------------------------------------------
bool MakeMyDay(int day1, int day2)
{
    return (day1 * day2) % 2;
}

// ---------------------------------------------------------------------------
// MakeSomeDay(day1:int):bool
// ---------------------------------------------------------------------------
bool MakeSomeDay(int day1)
{
    return day1 % 2;
}

void work1()
{
    return;
}